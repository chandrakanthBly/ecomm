System Requirements

1. Java 1.8
2. Spring Boot
3. MongoDB

Features (adding in progress)

1. Add user/account
POST API - localhost:8080/add/user

Body for the above request
```
{
	"userId":"user1",
	"contactNumber":"9999999",
	"address":{
		"line":"398, Domlur",
		"city":"Bengaluru",
		"state":"Karnataka",
		"country":"IN",
		"postalCode":"560071"
	}
}
```

2. Add product

POST API - localhost:8080/add/product

Body for above request
```
{
	"productId":"product1",
	"productName":"Headphones",
	"seller":"Sony",
	"sellerAddress":{
		"line":"Sony Corp",
		"city":"Kyoto",
		"state":"Kyoto",
		"country":"Japan",
		"postalCode":"5200465"
	},
	"listedPrice":300,
	"isVisible":true,
	"isLocked":false,
	"availableUnits":100
	
}
```
3. Create order
POST API - localhost:8080/create

Body for above request
```
{
		"quantityMap" : {
			"product1":20
		}
}
```