package com.spring.ecomm.exceptionHandler;

public class Exception extends  RuntimeException{
    private String message;

    public Exception(String message) {
        super(message);
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

}