package com.spring.ecomm.services;

import com.spring.ecomm.entities.User;
import com.spring.ecomm.entities.Product;
import com.spring.ecomm.entities.OrderHelper;
import com.spring.ecomm.entities.Cart;
import com.spring.ecomm.entities.Orders;
import com.spring.ecomm.exceptionHandler.Exception;
import com.spring.ecomm.repositories.UserRepository;
import com.spring.ecomm.repositories.ProductRepository;
import com.spring.ecomm.repositories.OrderRepository;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class CreationService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    ProductRepository productRepository;

    @Autowired
    OrderRepository orderRepository;

    public void addUser(User user){
        userRepository.save(user);
    }

    public  void addProduct(Product product){
        productRepository.save(product);
    }

    //TODO - adding orders
    public void createOrder(String accountId, Cart cart){
        if(validateUserId(accountId)) {
            Orders order = new Orders();
            List<OrderHelper> products = new ArrayList<>();
            Map<String, Integer> items = cart.getQuantityMap();
            double orderTotal = 0;
            for (Map.Entry mapElement : items.entrySet()) {
                Product product = productRepository.findByProductId((String) mapElement.getKey());
                if (product != null && product.isVisible() && !product.isLocked()) {
                    OrderHelper orderHelper = new OrderHelper();
                    orderHelper.setProductName(product.getProductName());
                    orderHelper.setProductId(product.getProductId());
                    orderHelper.setSellerAddress(product.getSellerAddress());
                    orderHelper.setPricePerItem(product.getListedPrice());
                    orderTotal += ((int) mapElement.getValue() * product.getListedPrice());
                    products.add(orderHelper);
                    productItemCountUpdate(product, (int) mapElement.getValue());
                } else {
                    throw new Exception("Product " + product.getProductId() + " " + product.getProductName() + "is not available!");
                }
            }

            String billNo = createOrderNo();
            order.setOrderId(billNo);
            order.setOrderTotal(orderTotal);
            order.setAccountId(accountId);
            order.setProductDetails(products);
            orderRepository.save(order);
        }
        else{
            throw new Exception("User/Account not found!");
        }
    }

    private boolean validateUserId(String accountId){
        User user = userRepository.findByUserId(accountId);
        if(user == null)
            return false;
        return true;
    }

    private void productItemCountUpdate(Product product, int units) {
        if(product.getAvailableUnits() - units <= 1)
            product.setLocked(true);
        else
            product.setLocked(false);
        product.setAvailableUnits(product.getAvailableUnits() - units);
        productRepository.save(product);
    }

    private String createOrderNo(){
        return "Order" + RandomStringUtils.randomAlphanumeric(7);
    }
}
