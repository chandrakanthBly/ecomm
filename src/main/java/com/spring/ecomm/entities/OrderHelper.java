package com.spring.ecomm.entities;

public class OrderHelper {
    private String productName;
    private String productId;
    private Address sellerAddress;
    private int pricePerItem;

    public OrderHelper(String name, String id, Address seller, int price){
        productName = name;
        productId = id;
        sellerAddress = seller;
        pricePerItem = price;
    }
    public OrderHelper(){}

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Address getSellerAddress() {
        return sellerAddress;
    }

    public void setSellerAddress(Address sellerAddress) {
        this.sellerAddress = sellerAddress;
    }

    public int getPricePerItem() {
        return pricePerItem;
    }

    public void setPricePerItem(int pricePerItem) {
        this.pricePerItem = pricePerItem;
    }

}