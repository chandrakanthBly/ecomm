package com.spring.ecomm.entities;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;

@Data
@Document
@JsonInclude(JsonInclude.Include.NON_NULL)
public class User {
    @Id
    private String userId;

    @NotNull
    private String contactNumber;

    @NotNull
    private Address address;

    public User(){}

    public User(String id, String number, Address add){
        userId = id;
        contactNumber = number;
        address = add;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}