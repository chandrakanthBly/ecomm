package com.spring.ecomm.entities;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import java.util.*;

@Data
@Document
@JsonInclude(Include.NON_NULL)

public class Orders {
    @Id
    private String orderId;
    private String accountId;
    private List<OrderHelper> productDetails;
    private Address deliveryAddress;
    private double orderTotal;

    public Orders(){
        orderTotal = 0.0;
    }

    public List<OrderHelper> getProductDetails() {
        return productDetails;
    }

    public void setProductDetails(List<OrderHelper> productDetails) {
        this.productDetails = productDetails;
    }

    public Address getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(Address deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public double getOrderTotal() {
        return orderTotal;
    }

    public void setOrderTotal(double orderTotal) {
        this.orderTotal = orderTotal;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }
}