package com.spring.ecomm.entities;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import java.util.Map;
import java.util.HashMap;

@JsonInclude(Include.NON_NULL)

public class Cart {
    Map<String, Integer> quantityMap;

    public Cart(){
        quantityMap = new HashMap<>();
    };

    public Cart(Map<String, Integer> items){
        quantityMap = items;
    }

    public Map<String, Integer> getQuantityMap() {
        return quantityMap;
    }

    public void setQuantityMap(Map<String, Integer> quantityMap) {
        this.quantityMap = quantityMap;
    }
}