package com.spring.ecomm.entities;

public class Address{
    private String line;
    private String city;
    private String state;
    private String country;
    private String pinCode;

    public Address(){}

    public Address(String line, String city, String state, String country, String pin) {
        this.line = line;
        this.city = city;
        this.state = state;
        this.country = country;
        pinCode = pin;
    }

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }
}