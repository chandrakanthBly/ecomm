package com.spring.ecomm.entities;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;

@Data
@Document
@JsonInclude(Include.NON_NULL)

public class Product {
    @Id
    private String productId;

    @NotNull
    private String productName;

    @NotNull
    private int listedPrice;

    @NotNull
    private int availableUnits;

    @NotNull
    private String seller;

    @NotNull
    private Address sellerAddress;

    private boolean isVisible;
    private boolean isLocked;

    public Product(){
        isVisible = false;
        isLocked = true;
    }

    public Product(String id, String name, int price, int units, String seller, Address sellerAddress, boolean visible, boolean locked){
        productId = id;
        productName = name;
        listedPrice = price;
        availableUnits = units;
        this.seller = seller;
        this.sellerAddress = sellerAddress;
        isVisible = visible;
        isLocked = locked;
    }

    public Address getSellerAddress() {
        return sellerAddress;
    }

    public void setSellerAddress(Address sellerAddress) {
        this.sellerAddress = sellerAddress;
    }

    public boolean isLocked() {
        return isLocked;
    }

    public void setLocked(boolean locked) {
        isLocked = locked;
    }

    public boolean isVisible() {
        return isVisible;
    }

    public void setVisible(boolean visible) {
        isVisible = visible;
    }

    public String getSeller() {
        return seller;
    }

    public void setSeller(String seller) {
        this.seller = seller;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getListedPrice() {
        return listedPrice;
    }

    public void setListedPrice(int listedPrice) {
        this.listedPrice = listedPrice;
    }

    public int getAvailableUnits() {
        return availableUnits;
    }

    public void setAvailableUnits(int availableUnits) {
        this.availableUnits = availableUnits;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }
}