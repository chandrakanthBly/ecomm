package com.spring.ecomm;

import com.spring.ecomm.entities.Cart;
import com.spring.ecomm.entities.Product;
import com.spring.ecomm.entities.Orders;
import com.spring.ecomm.entities.User;
import com.spring.ecomm.services.CreationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
public class ECommController {

    @Autowired
    CreationService createService;

    @GetMapping(value = "/health")
    public ResponseEntity getHealth() {
        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping(value = "/create")
    public ResponseEntity createOrder(@RequestHeader(value = "token") String token, @RequestBody Cart cart) {
        createService.createOrder(token,cart);
        return new ResponseEntity(HttpStatus.OK);
    }
    @PostMapping(value = "/add/user")
    public ResponseEntity newUser(@RequestBody User user){
        createService.addUser(user);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping(value = "/add/product")
    public ResponseEntity newProduct(@RequestBody Product product){
        createService.addProduct(product);
        return new ResponseEntity(HttpStatus.OK);
    }
}


