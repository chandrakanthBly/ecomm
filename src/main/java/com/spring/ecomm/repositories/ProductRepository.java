package com.spring.ecomm.repositories;

import com.spring.ecomm.entities.Product;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.mongodb.repository.MongoRepository;

import javax.persistence.LockModeType;

public interface ProductRepository extends MongoRepository<Product, String> {
    @Lock(LockModeType.PESSIMISTIC_READ)
    Product findByProductId(String productId);

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    Product save(Product product);
}