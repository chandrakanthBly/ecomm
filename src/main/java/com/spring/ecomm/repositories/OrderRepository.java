package com.spring.ecomm.repositories;

import com.spring.ecomm.entities.Orders;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface OrderRepository extends MongoRepository<Orders, String> {
    Orders findByOrderId(String orderId);
}